<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConferenceController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return new Response(<<<EOF
        <html>
            <body>
                <img src="images/logo.jpg" style="padding-left:37%"/>
                <img src="images/under-construction.gif" style="padding-left:43%"/>
            </body>
        </html>
        EOF);
    }
}
